FROM alpine:latest

COPY ./doviz.py /

RUN chmod +x /doviz.py && \
    apk update && \
    apk --no-cache add \
       py3-requests \
       py3-flask \
       py3-simplejson && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/doviz.py"]