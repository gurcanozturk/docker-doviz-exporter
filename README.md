# Prometheus icin dockerized doviz exporter

# Build
```
docker build -t doviz-exporter .
```
# Run
```
docker run -d --name doviz-exporter -p 7000:5000 -e APP_PORT=5000 -e DOVIZ="USD,EUR,GBP" -e ISLEM="alis,satis" doviz-exporter:latest
```

# Test
```
curl localhost:7000/metrics

# HELP usd_satis USD SATIS
# TYPE usd_satis gauge
usd_satis 18.8499
# HELP usd_alis USD ALIS
# TYPE usd_alis gauge
usd_alis 18.8406
# HELP eur_satis EUR SATIS
# TYPE eur_satis gauge
eur_satis 20.2907
# HELP eur_alis EUR ALIS
# TYPE eur_alis gauge
eur_alis 20.2852
# HELP gbp_satis GBP SATIS
# TYPE gbp_satis gauge
gbp_satis 22.9734
# HELP gbp_alis GBP ALIS
# TYPE gbp_alis gauge
gbp_alis 22.9644
```

# Environment Variables
| Variable | Description |
|------|------|
| APP_PORT | Application port in container, default 5000 |
| DOVIZ | Goruntulenmek istenen dovizler. (3 karakterli doviz kisa kodlari virgul ile ayrilarak girilebilir.) |
| ISLEM | Goruntulenmek istenen doviz islemleri (alis ve/veya satis, her ikisi de isteniyorsa virgul ile ayrilmalidir.) |

# Scrape it
Ayni networkteki prometheus icin yapilandirma.

```
### Doviz exporter
  - job_name: "doviz"
    scrape_interval: 2m
    scrape_timeout: 2m
    static_configs:
    - targets: ["doviz-exporter:5000"] # container_name:APP_PORT
      labels:
        alias: 'doviz'
```