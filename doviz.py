#!/usr/bin/python3

import os
import json
import requests
from flask import Flask
from flask import request

APP_PORT = os.environ['APP_PORT']
dovizler = os.environ['DOVIZ'].split(",")
islemler = os.environ['ISLEM'].lower().split(",")
results  = {}

url = "https://api.genelpara.com/embed/para-birimleri.json"
response = json.loads(requests.get(url).text)

def get_results():
  for currency in response:
    if currency in dovizler:
      keys = response[currency].keys()
      for key in keys:
        if key in islemler:
          result = ("%s_%s"  % (currency.lower(), key))
          results[result] = response[currency][key]

  last = ""
  for key, value in results.items():
    text  = ("%s" % (key.replace('_', ' ').upper()))
    line  = ("# HELP %s %s\n" % (key, text))
    line += ("# TYPE %s gauge\n" % (key))
    line += ("%s %s\n" % (k, "{:.3f}".format(float(value))))
    last += line
  return last.strip('\n')


app = Flask(__name__)

@app.route('/metrics')
def index():
  results = get_results()
  return results

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=APP_PORT)